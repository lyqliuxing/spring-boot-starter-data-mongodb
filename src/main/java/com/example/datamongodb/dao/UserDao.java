package com.example.datamongodb.dao;

import com.example.datamongodb.entity.UserEntity;

public interface UserDao {
	public void saveUser(UserEntity user);

    public UserEntity findUserByUserName(String userName);

    public int updateUser(UserEntity user);

    public void deleteUserById(Long id);
}
